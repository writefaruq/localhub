# Copyright (c) 2020 by Dan Jacob
# SPDX-License-Identifier: AGPL-3.0-or-later

from django.db import models
from django.utils.translation import gettext as _

from localhub.activities.models import Activity
from localhub.db.search import SearchIndexer
from localhub.db.tracker import Tracker
from localhub.utils.http import get_domain, is_https, is_image_url


class Post(Activity):

    RESHARED_FIELDS = Activity.RESHARED_FIELDS + (
        "opengraph_description",
        "opengraph_image",
        "url",
    )

    title = models.CharField(max_length=300, blank=True)
    url = models.URLField(max_length=500, blank=True)

    # metadata fetched from URL if available

    opengraph_image = models.URLField(max_length=500, blank=True)
    opengraph_description = models.TextField(blank=True)

    url_tracker = Tracker(["url"])

    search_indexer = SearchIndexer(("A", "title"), ("B", "indexable_description"))

    def __str__(self):
        return self.title or self.get_domain() or _("Post")

    def get_domain(self):
        return get_domain(self.url) or ""

    @property
    def indexable_description(self):
        return " ".join(
            [value for value in (self.description, self.opengraph_description) if value]
        )

    def get_opengraph_image_if_safe(self):
        """
        Returns metadata image if it is an https URL and a valid image extension.
        Otherwise returns empty string.
        """
        return (
            self.opengraph_image
            if is_https(self.opengraph_image) and is_image_url(self.opengraph_image)
            else ""
        )
