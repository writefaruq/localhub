# Generated by Django 2.2 on 2019-05-30 10:27

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("communities", "0002_auto_20190530_0922"),
    ]

    operations = [
        migrations.AddField(
            model_name="community",
            name="email_domain",
            field=models.CharField(
                blank=True,
                max_length=100,
                null=True,
                validators=[
                    django.core.validators.RegexValidator(
                        message="This is not a valid domain",
                        regex="([a-z¡-\uffff0-9](?:[a-z¡-\uffff0-9-]{0,61}[a-z¡-\uffff0-9])?(?:\\.(?!-)[a-z¡-\uffff0-9-]{1,63}(?<!-))*\\.(?!-)(?:[a-z¡-\uffff-]{2,63}|xn--[a-z0-9]{1,59})(?<!-)\\.?|localhost)",
                    )
                ],
            ),
        ),
    ]
