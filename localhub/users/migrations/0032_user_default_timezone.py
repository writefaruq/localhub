# Generated by Django 2.2.4 on 2019-09-08 13:30

import timezone_field.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("users", "0031_auto_20190830_1223"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="default_timezone",
            field=timezone_field.fields.TimeZoneField(default="UTC"),
        ),
    ]
