# Generated by Django 2.2.2 on 2019-07-05 21:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("photos", "0002_auto_20190705_1857"),
    ]

    operations = [
        migrations.AddField(
            model_name="photo",
            name="artist",
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name="photo",
            name="cc_license",
            field=models.CharField(
                blank=True,
                choices=[
                    ("by", "Attribution"),
                    ("by-sa", "Attribution ShareAlike"),
                    ("by-nd", "Attribution NoDerivs"),
                    ("by-nc", "Attribution NonCommercial"),
                    ("by-nc-sa", "Attribution NonCommercial ShareAlike"),
                    ("by-nc-nd", "Attribution NonCommercial NoDerivs"),
                ],
                max_length=10,
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="photo",
            name="original_url",
            field=models.URLField(blank=True, null=True),
        ),
    ]
