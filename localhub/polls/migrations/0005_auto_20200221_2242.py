# Generated by Django 3.0.3 on 2020-02-21 22:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("polls", "0004_auto_20191121_1805"),
    ]

    operations = [
        migrations.AddField(
            model_name="historicalpoll",
            name="edited",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="poll",
            name="edited",
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
