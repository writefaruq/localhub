# Generated by Django 2.2.5 on 2019-09-15 20:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("polls", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="historicalpoll",
            name="title",
            field=models.CharField(max_length=300),
        ),
        migrations.AlterField(
            model_name="poll", name="title", field=models.CharField(max_length=300),
        ),
    ]
