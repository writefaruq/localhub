# Generated by Django 3.0.3 on 2020-02-11 21:08

from django.db import migrations


def insert_replies_for_thread(Message, thread, parent):
    children = Message.objects.filter(parent=parent)
    children.update(thread=thread)
    for child in children:
        insert_replies_for_thread(Message, thread, child)


def insert_threads(apps, schema_editor):
    Message = apps.get_model("private_messages", "Message")
    for message in Message.objects.filter(parent__isnull=True):
        insert_replies_for_thread(Message, message, message)


def remove_threads(apps, schema_editor):
    Message = apps.get_model("private_messages", "Message")
    Message.objects.update(thread=None)


class Migration(migrations.Migration):

    dependencies = [
        ("private_messages", "0007_auto_20200211_2103"),
    ]

    operations = [migrations.RunPython(insert_threads, reverse_code=remove_threads)]
