# Generated by Django 3.0.3 on 2020-02-11 21:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("join_requests", "0005_joinrequest_intro"),
    ]

    operations = [
        migrations.AlterField(
            model_name="joinrequest",
            name="intro",
            field=models.TextField(
                blank=True,
                help_text="Tell us a little bit about yourself and why you would like to join this community (Optional).",
                verbose_name="Introduction",
            ),
        ),
    ]
